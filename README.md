# PHP Bitbucket API

[![Latest Version](https://img.shields.io/packagist/v/gentle/bitbucket-api.svg?style=flat-square)](https://packagist.org/packages/gentle/bitbucket-api)
[![License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://bitbucket.org/gentlero/bitbucket-api/src/master/LICENSE)
[![Build Status](https://img.shields.io/travis/com/gentlero/bitbucket-api?style=flat-square)](https://travis-ci.com/bitbucket/gentlero/bitbucket-api)
[![Coverage Status](https://img.shields.io/scrutinizer/coverage/b/gentlero/bitbucket-api.svg?style=flat-square)](https://scrutinizer-ci.com/b/gentlero/bitbucket-api/?branch=develop)
[![Code quality](https://img.shields.io/scrutinizer/b/gentlero/bitbucket-api.svg?style=flat-square)](https://scrutinizer-ci.com/b/gentlero/bitbucket-api/?branch=develop)

Simple Bitbucket API wrapper for PHP

## Requirements

* PHP >= 7.1 with [cURL](http://php.net/manual/en/book.curl.php) extension.
* [Buzz](https://github.com/kriswallsmith/Buzz) library or any [HTTPlug](http://httplug.io/) compatible http client,
* PHPUnit to run tests. ( _optional_ )

## Getting started

Read [Introduction](https://gentlero.bitbucket.io/bitbucket-api/#introduction) and [Installation](https://gentlero.bitbucket.io/bitbucket-api/installation.html) from the documentation.

## Documentation

See [https://gentlero.bitbucket.io/bitbucket-api/](https://gentlero.bitbucket.io/bitbucket-api/) for more detailed documentation.

## License

`bitbucket-api` is licensed under the MIT License - see the LICENSE file for details

## Contribute

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

For any security related issues, please send an email at [alex@gentle.ro][maintainer-pgp] instead of using the issue tracker.

[maintainer-pgp]: https://keybase.io/vimishor/key.asc
